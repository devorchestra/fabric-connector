import {BlockData} from "fabric-client";

/**
 * CA Administrator connection class, which allows to register new identities
 */
declare class AdminCAConnection extends BaseFabricConnection {
    /**
     * @param {Object} options
     * @param {string} options.connectionName - name of the client
     * @param {string} options.caName - name of the client's CA
     * @param {string} options.orgName - name of the client's Organization
     * @param {Object} options.connectionProfile - entries that describe the Fabric network
     * @param {ClientCredentials} options.credentials - (optional) client's credentials for Fabric network
     * @param {boolean} options.tls - (optional) enable/disable mutual tls secure connection
     *
     */
    constructor(options: { connectionName: string, caName: string, orgName: string, connectionProfile: any, credentials?: ClientCredentials })

    /**
     *
     * @param {Object} clientData - data of new identity that need to register
     * @param {string} clientData.enrollmentID - ID which will be used for enrollment
     * @param {string} clientData.enrollmentSecret - (optional) enrollment secret to set for the registered user.
     *                    If not provided, the server will generate one.
     * @param {string} clientData.role - Role value of the identity (client, peer, orderer)
     * @param {string} clientData.affiliation - Affiliation with which this user will be associated,
     *                    like a company or an organization
     * @param {number} clientData.maxEnrollments - The maximum number of times this user will be permitted to enroll
     * @param {KeyValueAttribute[]} clientData.attrs - (optional) Array of {@link KeyValueAttribute} attributes to assign to the identity
     *
     * @return {Promise<RegisteredIdentityCredentials>}
     */
    registerClientIdentity(clientData: { enrollmentID: string, enrollmentSecret?: string, affiliation: string, role: string, maxEnrollments: number, attrs?: KeyValueAttribute[] }): Promise<RegisteredIdentityCredentials>


    /**
     * List CA affiliations
     * @return {Promise<AffiliationObject[]>}
     */
    listAffiliations(): Promise<AffiliationObject[]>

    /**
     * Add affiliation to CA
     *
     * @param {Object} req
     * @param {string} req.name - name of affiliation to create (e.g. dep1.subdep1)
     * @param {string} req.caname
     * @return {Promise<Object>} - returns successful result info
     */
    addAffiliation(req: {name: string, caname: string}): Promise<Object>

    /**
     * Delete affiliation to CA
     *
     * @param {Object} req
     * @param {string} req.name - name of affiliation to delete (e.g. dep1.subdep1)
     * @param {string} req.caname
     * @return {Promise<Object>} - returns successful result info
     */
    deleteAffiliation(req: {name: string, caname: string}): Promise<Object>
}

/**
 * Remote client connection
 */
declare class OuterClientConnection extends BaseFabricConnection {

    /**
     * @param {Object} options
     * @param {string} options.connectionName - name of the client connection
     * @param {string} options.orgName - name of the client's Organization
     * @param {Object} options.connectionProfile - entries that describe the Fabric network
     * @param {ClientCredentials} options.tlsClientCredentials - (optional) enable/disable mutual tls secure connection
     * @param {TimeoutsConfig} options.timeouts - (optional) default timeouts for all requests to Fabric in ms
     *
     */
    constructor(options: { connectionName: string, orgName: string, connectionProfile: any, tlsClientCredentials?: ClientCredentials, timeouts?: TimeoutsConfig })

    /**
     * Need to invoke for connection initialization
     * @param {Object} options
     * @param {TxObserver} options.txObserverInstance - instance of {@link TxObserver class, which is responsible to listen for committed transactions
     * @param {ServiceDiscoveryProvider} options.serviceDiscoveryProvider (optional) - instance of {@link ServiceDiscoveryProvider class. If passed, service discovery feature will be automatically enabled
     *
     * @return {Promise<null>}
     */
    init(options: { txObserverInstance: TxObserver, serviceDiscoveryProvider?: ServiceDiscoveryProvider } ): Promise<null>

    /**
     * Generate unsigned proposal for client
     * @param {Object} req - payload of proposal
     * @param {string} req.chaincodeId - chaincode name
     * @param {string} req.clientCertificate - PEM-encoded client certificate
     * @param {string} req.fcn - function name in chaincode
     * @param {string[]} req.args - arguments to pass to function
     * @param {string} req.channelId - channel name
     *
     * @return {Promise<GenerateUnsignedProposalResponse>}
     */
    generateUnsignedProposal(req: { chaincodeId: string, clientCertificate: string, fcn: string, args: Array<string>, channelId: string }): Promise<GenerateUnsignedProposalResponse>

    /**
     * Send signed proposal by remote client to peers
     * @param {Object} req - payload of proposal
     * @param {string} req.channelId - channel name
     * @param {string} req.chaincodeId - chaincode name
     * @param {Object} req.signedProposal -  proposal signed by client
     * @param {string[]} req.targets - (optional) array of peers' name from connection profile

     * @return {Promise<SendSignedProposalResponse[]>}
     */
    sendSignedProposal(req: { channelId: string, chaincodeId: string, signedProposal: any, targets?: string[] }): Promise<SendSignedProposalResponse[]>

    /**
     * Generate unsigned transaction for client to send to Orderers
     * @param {Object} req - payload of proposal
     * @param {string} req.channelId - channel name
     * @param {Object} req.signedProposal -  proposal signed by client
     *
     * @return {Promise<Object>}
     */
    generateUnsignedTransaction(req: { channelId: string, proposal: any, proposalResponses: any }): Promise<any>

    /**
     * Send transaction signed by client to orderer
     * @param {Object} req - payload of proposal
     * @param {string} req.channelId - channel name
     * @param {Object} req.signedTransactionRequest -  transaction signed by client
     * @param {Object} req.transactionRequest -  original transaction
     * @param {string} req.orderer - orderer name from connection profile
     * @param {string} req.txId - transaction id
     *
     * @return {Promise<ListenTxCommitmentResponse>}
     */
    sendSignedTransaction(req: { channelId: string, signedTransactionRequest: any, transactionRequest: any, txId: string, orderer?: string }): Promise<ListenTxCommitmentResponse>

    /**
     * Query chaincode with proposal signed by client
     * @param {Object} req - payload of proposal
     * @param {string} req.channelId - channel name
     * @param {Object} req.signedProposal -  proposal signed by client
     * @param {string} req.target - (optional) peer's name from connection profile
     *
     * @return {Promise<QueryChaincodeResponse>}
     */
    queryWithSignedProposal(req: { channelId: string, signedProposal: any, target?: string }): Promise<QueryChaincodeResponse>
}


/**
 *  Common client connection class, which allows to execute transactions and making queries
 */
declare class ClientConnection extends BaseFabricConnection {

    /**
     * Need to invoke for connection initialization
     * @param {Object} options (optional)
     * @param {TxObserver} options.txObserverInstance (optional) - instance of {@link TxObserver class, which is responsible to listen for committed transactions
     * @param {ServiceDiscoveryProvider} options.serviceDiscoveryProvider (optional) - instance of {@link ServiceDiscoveryProvider class. If passed, service discovery feature will be automatically enabled

     *
     * @return {Promise<null>}
     */
    init(options?: { txObserverInstance?: TxObserver, serviceDiscoveryProvider?: ServiceDiscoveryProvider }): Promise<null>

    /**
     * Execute transaction in chaincode
     * @param {Object} req - payload of transaction
     * @param {string} req.chaincodeId - chaincode name
     * @param {string} req.fcn - function name in chaincode
     * @param {string[]} req.args - arguments to pass to function
     * @param {string} req.channelId - channel name
     * @param {string[]} req.targets - (optional) array of peers' name from connection profile
     * @param {string} req.orderer - (optional) orderer's name from connection profile
     * @return {Promise<ExecuteChaincodeResponse>}
     */
    executeTxInChaincode(req: { chaincodeId: string, fcn: string, args: string[], channelId: string, targets?: string[], orderer: string }): Promise<ExecuteChaincodeResponse>

    /**
     * Query chaincode
     * @param {Object} req - payload of query
     * @param {string} req.chaincodeId - chaincode name
     * @param {string} req.fcn - function name in chaincode
     * @param {string[]} req.args - arguments to pass to function
     * @param {string} req.channelId - channel name
     * @param {string} req.target - (optional) peer's name from connection profile
     *
     * @return {Promise<QueryChaincodeResponse>}
     */
    queryInChaincode(req: { chaincodeId: string, fcn: string, args: string[], channelId: string, target?: string }): Promise<QueryChaincodeResponse>
}

/**
 * Connection that subscribes to new blocks of the channel
 */
declare class ChannelListenerConnection extends BaseFabricConnection {

    /**
     * @param {Object} options
     * @param {string} options.connectionName - name of the client
     * @param {string} options.channelId - channel name
     * @param {string} options.orgName - name of the client's Organization
     * @param {Object} options.connectionProfile - entries that describe the Fabric network
     * @param {ClientCredentials} options.credentials (optional) - client's credentials for Fabric network
     * @param {boolean} options.tls - (optional) enable/disable mutual tls secure connection
     *
     */
    constructor(options: { connectionName: string, channelId: string, orgName: string, connectionProfile: any, tls?: boolean, credentials: ClientCredentials })

    /**
     * Start listen ti new blocks
     * @param {Object} options
     * @param {int} options.startBlock - (optional) The starting block number
     *           for event checking. When included, the peer's fabric service
     *           will be asked to start sending blocks from this block number.
     *           This is how to resume or replay missed blocks that were added
     *           to the ledger.
     *           Default is the latest block on the ledger.
     * @param {boolean} options.fullBlocks - is need to receive full blocks or not
     *
     * @return {Promise<null>}
     */
    establishConnection(options?: { startBlock?: number, fullBlocks?: boolean }): Promise<null>

    /**
     * Stop listen to ne blocks
     */
    disconnect(): void

    on(event: 'block', listener: (newBlock: any) => void): this;

    on(event: 'connected', listener: ({channelId, peerName}: {channelId: string, peerName: string}) => void): this;

    on(event: 'disconnected', listener: ({channelId, peerName, reason}: {channelId: string, peerName: string, reason: any}) => void): this;

    /**
     * Will be emitted when ChannelListenerConnection stops trying to reconnect to provided peers.
     * This event strictly has to be handled
     */
    on(event: 'closed', listener: ({channelId, reason}: {channelId: string, reason: any}) => void): this;

}

/**
 * Basic class for Fabric's clients connections. All new classes of connections should be inherited from this class.
 */
declare class BaseFabricConnection {

    /**
     * @param {Object} options
     * @param {string} options.connectionName - name of the client
     * @param {string} options.caName - (optional) name of the client's CA
     * @param {string} options.orgName - name of the client's Organization
     * @param {Object} options.connectionProfile - entries that describe the Fabric network
     * @param {ClientCredentials} options.credentials (optional) - client's credentials for Fabric network
     * @param {boolean} options.tls - (optional) enable/disable mutual tls secure connection
     * @param {TimeoutsConfig} options.timeouts - (optional) default timeouts for all requests to Fabric in ms
     *
     */
    constructor(options: { connectionName: string, caName?: string, orgName: string, connectionProfile: any, tls?: boolean, timeouts?: TimeoutsConfig, credentials?: ClientCredentials })

    /**
     * Sets config field 'request-timeout' in fabric-sdk library. Strictly required to set as this value is used for some requests to orderer
     * because some methods don't support custom timeout. See AdminMSPConnection to know which methods.
     * @param timeout - time in ms
     */
    static setRequestTimeout(timeout): void

    /**
     * Sets config field 'connection-timeout' in fabric-sdk library which is used by requests to CA. Strictly required to set
     * as this is the only way to set custom timeout for interaction with CA.
     * @param timeout - time in ms
     */
    static setCARequestTimeout(timeout): void


    /**
     * Need to invoke for connection initialization
     * @param {Object} options - (optional)
     *
     * @return {Promise<null>}
     */
    init(options?: {}): Promise<null>

    /**
     * Need to invoke for connection initialization without client's credentials. It is used for client enrollment
     * @return {Promise<null>}
     */
    initWithoutCredentials(): Promise<null>


    /**
     * Enroll a registered user in order to receive a signed X509 certificate
     * @param {Object} credentials
     * @param {string} credentials.enrollmentID - The registered ID to use for enrollment
     * @param {string} credentials.enrollmentSecret - The secret associated with the enrollment ID
     * @param {AttributeRequest[]} credentials.attrReqs - list of attributes

     * @returns {Promise<ClientCredentials>}
     */
    enroll(credentials: { enrollmentID: string, enrollmentSecret: string, attrReqs?: any[] }): Promise<ClientCredentials>


    /**
     * Enroll a registered user in order to receive a signed X509 certificate
     * @param {Object} credentials
     * @param {string} credentials.enrollmentID - The registered ID to use for enrollment
     * @param {string} credentials.enrollmentSecret - The secret associated with the enrollment ID
     * @param {string} credentials.csr - PEM-encoded PKCS#10 certificate signing request
     * @param {AttributeRequest[]} credentials.attrReqs - list of attributes
     *
     * @returns {Promise<ClientCredentials>}
     */
    enrollWithCSR(credentials: { enrollmentID: string, enrollmentSecret: string, csr: string, attrReqs?: any[] }): Promise<ClientCredentials>

    /**
     * Return client credentials of established connection
     * @returns {Promise<ClientCredentials>}
     */
    getCredentials(): Promise<ClientCredentials>
}

/**
 * Connection with methods for managing Fabric network
 */
declare class AdminMSPConnection extends ClientConnection {

    /**
     * Extracts the protobuf 'ConfigUpdate' object out of the 'ConfigEnvelope' object that is produced by the configtxgen tool.

     * @param {Buffer} configEnvelope - The encoded bytes of the ConfigEnvelope protobuf
     * @return {Buffer} config - The encoded bytes of the ConfigUpdate protobuf, ready to be signed
     */
    buildSignableChannelConfig(configEnvelope: Buffer): Buffer

    /**
     *  Signs over the configuration bytes passed in
     * @param {Buffer} config - the channel configuration as Protobuff
     * @return {ConfigSignature} - signature objects
     */
    signChannelConfig(config: Buffer): ConfigSignature


    /**
     * Create channel from 'ConfigEnvelope' that is produced by the configtxgen tool
     * @param {string} channelId
     * @param {Buffer} configEnvelope - produced by the configtxgen tool
     * @param {string} orderer (optional) - orderer name from connection profile
     *
     * @return {Promise<BroadcastResponse>} - response from orderer, note that this is not the confirmation of successful creation of the channel,
     * The client application must poll the orderer to discover whether the channel has been created completely or not.
     */
    createChannelFromEnvelope(channelId: string, configEnvelope: Buffer, orderer?: string): Promise<BroadcastResponse>


    /**
     * Create channel from configuration object (see example in tests)
     * @param {string} channelId - name of the channel
     * @param {Object} config - configuration object
     * @param {string} orderer (optional) - orderer name from connection profile
     *
     * @return {Promise<BroadcastResponse>} - response from orderer, note that this is not the confirmation of successful creation of the channel,
     * The client application must poll the orderer to discover whether the channel has been created completely or not.
     */
    createChannel(channelId: string, config: Object, orderer?: string): Promise<BroadcastResponse>

    /**
     * Get user-friendly channel genesis block
     * @param channelId
     * @return {Promise<Object>}
     */
    getGenesisBlock(channelId: string): Promise<Object>

    /**
     * Get channel config as protobuff
     * @param req
     * @param {string} req.channelId
     * @param {string} req.target (optional) - peer name or url
     * @param {string} req.fromOrderer (optional) - flag to retrieve channel config from Orderer (not from peer)
     * @return {Promise<Buffer>} - protobuff config
     */
    getChannelConfigProtobuff(req: {channelId: string, target?: string, fromOrderer?: boolean}): Promise<Buffer>

    /**
     * Update channel config with 'UpdateConfig' as protobuff
     * @param req
     * @param {string} req.channelId
     * @param {Buffer} req.updateConfigProtobuff
     * @param {ConfigSignature[]} req.signatures
     *
     * @return {Promise<BroadcastResponse>} - response from orderer, note that this is not the confirmation of successful update of the channel
     */
    updateChannelConfig(req: {channelId: string, updateConfigProtobuff: Buffer, signatures: ConfigSignature[]}): Promise<BroadcastResponse>

    /**
     * Add anchor peers to the channel config for this organization
     *
     * @param {string} channelId
     * @param {PeerDefinition[]} anchorPeers
     * @param {string} orderer (optional) - orderer name from connection profile
     *
     * @return {Promise<BroadcastResponse>} - response from orderer, note that this is not the confirmation of successful update of the channel
     */
    addAnchorPeers(channelId: string, anchorPeers: PeerDefinition[], orderer?: string): Promise<BroadcastResponse>



    /**
     * Requests to peers to join channel
     * @param {string} channelId - name of the channel
     * @param {string[]} targets (optional) - array of peers' name from connection profile
     *
     * @return {Promise< Array<ProposalResponse | ProposalErrorResponse | Error> >} - result of requests
     */
    joinChannel(channelId: string, targets?: string[]): Promise< Array<ProposalResponse | ProposalErrorResponse | Error> >

    /**
     * Requests to peers to install chaincode
     * @param {Object} req - payload of the request
     * @param {string} req.chaincodeId - name of the chaincode
     * @param {string} req.chaincodeVersion - version of the chaincode to install
     * @param {string} req.chaincodePath - The path to the location of the source code of the chaincode. Note: GOPATH has to be set
     * @param {string[]} req.targets (optional) - array of peers' name from connection profile
     *
     * @return {Promise< Array<ProposalResponse | ProposalErrorResponse | Error> >} - result of requests
     */
    installChaincodeOnPeers(req: {chaincodeId: string, chaincodeVersion: string, chaincodePath: string, targets?: string[]}): Promise< Array<ProposalResponse | ProposalErrorResponse | Error> >

    /**
     * Request to upgrade chaincode on the channel
     * @param {Object} req - payload of the request
     * @param {string} req.channelId - name of the channel
     * @param {string} req.chaincodeId - name of the chaincode
     * @param {string} req.chaincodeVersion - version of the chaincode to install
     * @param {string} req.endorsementPolicy - chaincode endorsement policy
     * @param {string} req.target (optional) - peer name from connection profile
     *
     * @return {ChannelHubShortBlock}
     * @throws {ProposalErrorResponse | Error}
     */
    upgradeChaincodeOnChannel(req: {channelId: string, chaincodeId: string, chaincodeVersion: string, endorsementPolicy: Object, target: string}): Promise<ChannelHubShortBlock>

    /**
     * Request to instantiate chaincode on the channel
     * @param {Object} req - payload of the request
     * @param {string} req.channelId - name of the channel
     * @param {string} req.chaincodeId - name of the chaincode
     * @param {string} req.chaincodeVersion - version of the chaincode to install
     * @param {string} req.endorsementPolicy - chaincode endorsement policy
     * @param {string} req.target (optional) - peer name from connection profile
     *
     * @return {ChannelHubShortBlock}
     * @throws {ProposalErrorResponse | Error}
     */
    instantiateChaincodeOnChannel(req: {channelId: string, chaincodeId: string, chaincodeVersion: string, endorsementPolicy: Object, target: string}): Promise<ChannelHubShortBlock>

}

/**
 * Connection that's responsible for listening committed transactions to the channel
 */
declare class TxObserver extends BaseFabricConnection {

    /**
     * Need to invoke for connection initialization. Subscribes to event from channels, listed in connection profile
     * @return {Promise<null>}
     */
    startObserver(): Promise<null>

    /**
     * Need to invoke for disconnecting
     * @return {Promise<null>}
     */
    stopObserver(): Promise<null>

    /**
     * Start listen to transaction to be committed to the ledger
     * @param {string} channelId - channel name
     * @param {string} txId - transaction id
     * @param {number} timeout - (optional) ms
     *
     * @return {Promise<ListenTxCommitmentResponse>}
     */
    listenToTx(channelId: string, txId: string, timeout?: number): Promise<ListenTxCommitmentResponse>

    /**
     * Stop listen to transaction to be committed to the ledger
     * @param {string} channelId - channel name
     * @param {string} txId - transaction id
     */
    stopListenToTx(channelId: string, txId: string): void

    on(event: 'connected', listener: ({channelId, peerName}: {channelId: string, peerName: string}) => void): this;

    on(event: 'disconnected', listener: ({channelId, peerName, reason}: {channelId: string, peerName: string, reason: any}) => void): this;

    /**
     * Will be emitted when ChannelListenerConnection stops trying to reconnect to provided peers.
     * This event strictly has to be handled
     */
    on(event: 'closed', listener: ({channelId, reason}: {channelId: string, reason: any}) => void): this;
}

declare class Configtxlator {

    /**
     * @param url - address of configtxlator tool server
     * @param requestTimeout - http request timeout (in ms)
     */
    constructor(url, requestTimeout)

    /**
     * Encode config represented as object to protobuff format
     * @param {Object} config
     * @return {Promise<Buffer>} - config in protobuff format
     */
    encodeConfigToProtobuff(config: Object): Promise<Buffer>

    /**
     * Decode config in protobuff format to object
     * @param {Buffer} configProtobuff
     * @return {Promise<Object>}
     */
    decodeConfigProtobuff(configProtobuff: Buffer): Promise<Object>

    /**
     * Decode channel block to user-friendly object
     * @param {Block} block
     * @return {Promise<Object>}
     */
    decodeBlock(block: Block): Promise<Object>

    /**
     * generate channel creation protobuff based on channel creation config object
     * @param {Object} config
     * @return {Promise<Buffer>}
     */
    generateCreateChannelProtobuff(config: Object): Promise<Buffer>

    /**
     * generate channel update config protobuff based on original config
     * @param {string} channelId
     * @param {Buffer} originalConfigProtobuff
     * @param {Buffer} updatedConfigProtobuff
     *
     * @return {Promise<Buffer>}
     */
    generateUpdateConfigProtobuff(channelId: string, originalConfigProtobuff: Buffer, updatedConfigProtobuff: Buffer): Promise<Buffer>

}

/**
 * Responsible for providing results of service discovery. Also provides some methods for enhancement of fabric-sdk Channel class
 */
declare class ServiceDiscoveryProvider extends BaseFabricConnection {
    /**
     * Starts provider
     * @param options
     */
    start(options: any)
}

declare class MemoryKeyValueStore {
    constructor()

    getValue(name: string)

    setValue(name: string, value: any)

}

/**
 * Config object with timeout values for connection instances
 */
interface TimeoutsConfig {
    peerRequest?: number;
    peerChaincodeInstall?: number;
    ordererBroadcast?: number;
    txCommitment?: number;
}



/**
 * Client credentials to interact with Fabric network
 */
interface ClientCredentials {
    /**
     * pem-encoded certificate as string
     */
    certificate: string;
    /**
     * pem-encoded private key as string
     */
    privateKey: string;
}

/**
 * New identity id and password for enrollment
 */
interface RegisteredIdentityCredentials {
    /**
     * ID which will be used for enrollment
     */
    enrollmentID: string;
    /**
     * enrollment secret to set for the registered user.
     */
    enrollmentSecret: string;
}

/**
 * Attributes of identity in certificate
 */
interface AttributeRequest {
    /**
     * The name of the attribute to include in the certificate
     */
    name: string;
    /**
     * throw an error if the identity does not have the attribute
     */
    optional: boolean;
}

/**
 * Array of attributes to assign to the identity
 */
interface KeyValueAttribute {
    /**
     * The key used to reference the attribute
     */
    name: string;
    /**
     * The value of the attribute
     */
    value: string;
    /**
     * A value of true indicates that this attribute should be included in an enrollment certificate by default
     */
    ecert?: boolean
}

/**
 * Response on transaction execution in chaincode
 */
interface ExecuteChaincodeResponse {
    status: string;
    message: string;
    payload: any;
}

/**
 * Response on query in chaincode
 */
interface QueryChaincodeResponse {
    status: string;
    payload: any;
}

/**
 * Response for transaction has been committed to ledger
 */
interface ListenTxCommitmentResponse {
    channelId:string;
    code: string;
    blockNumber: string;
    txId: string;
}

/**
 * Object that client is needed to sign before sending proposal to peers
 */
interface GenerateUnsignedProposalResponse {
    txId: string;
    proposal: any;
}

/**
 * A protobuf message that gets returned by endorsing peers on proposal requests
 */
interface SendSignedProposalResponse {
    version: number;
    timestamp: any;
    response: any;
    payload: any;
    endorsement: any;
}

/**
 * A protobuf message that gets returned by endorsing peers on proposal requests
 */
interface ProposalResponse {
    version: number;
    timestamp: Date;
    response: InfoResponse;
    payload: Buffer;
    endorsement: any;
    peer: RemoteCharacteristicsFabricNode;
}
/**
 * A protobuf message that gets returned by endorsing peers on proposal requests
 */
interface ProposalErrorResponse {
    message: string
    status: number,
    payload: Buffer,
    peer: RemoteCharacteristicsFabricNode,
    isProposalResponse: boolean
}

interface InfoResponse {
    status: number;
    message: string;
    payload: Buffer;
}

interface RemoteCharacteristicsFabricNode {
    url: string;
    name: string;
    options: object;
}

/**
 * The signature of the identity on the config bytes
 */
interface ConfigSignature {
    signature_header: Buffer;
    signature: Buffer;
}

/**
 * Response from orderer for sending proposal
 */
interface BroadcastResponse {
    status: string;
    info?: string;
}

/**
 * Short blocks from channel event hub
 */
interface ChannelHubShortBlock {
    channelId: string,
    code: string,
    txId: string,
    block: number
}

interface AffiliationObject {
    name: string,
    affiliations?: AffiliationObject[]
}

export interface Block {
    header: {
        number: number;
        previous_hash: Buffer;
        data_hash: Buffer;
    };
    data: { data: BlockData[] };
    metadata: { metadata: any };
}

export interface PeerDefinition {
    host: string,
    port: number
}

export {AdminCAConnection};
export {OuterClientConnection};
export {ClientConnection};
export {BaseFabricConnection};
export {ChannelListenerConnection};
export {MemoryKeyValueStore};
export {TxObserver};
export {AdminMSPConnection};
export {Configtxlator};
export {ServiceDiscoveryProvider};