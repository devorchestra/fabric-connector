let AdminConnection = require('../src/AdminMSPConnection');
let Configtxlator = require('../src/Configtxlator');


let mspAdmin = "admin-msp";
let configtxlatorURL = 'http://127.0.0.1:7059';
let channelId = "admin-msp-channel-1";
let orgName = "Org1";
let tls = true;
let mspAdminCredentials = require(`./${mspAdmin}-${orgName.toLowerCase()}Credentials.json`);
let connectionProfile = require(`./connectionProfile-${orgName.toLowerCase()}.json`);

let ordererAdmin = "orderer-admin";
let ordererAdminCredentials = require(`./${ordererAdmin}Credentials.json`);



let configtxlator = new Configtxlator(configtxlatorURL);
let connection = new AdminConnection({connectionName: mspAdmin, orgName, connectionProfile, credentials: mspAdminCredentials, tls});

let originalConfigProto;
let ordererConnection = new AdminConnection({connectionName: ordererAdmin, orgName: "Orderer", connectionProfile, credentials: ordererAdminCredentials, tls});

Promise.resolve()
    .then(() => {
        return Promise.all([
            connection.init(),
            ordererConnection.init()
        ])
    })
    .then(() => {

        return connection.getChannelConfigProtobuff({channelId});
    })
    .then((configProtobuff) => {
        originalConfigProto = configProtobuff;
        return configtxlator.decodeConfigProtobuff(originalConfigProto);
    })
    .then((config) => {
        config.channel_group.groups.Orderer.values.BatchSize.value.max_message_count = 20;
        return configtxlator.encodeConfigToProtobuff(config);
    })
    .then((updatedConfigProtobuff) => {
        return configtxlator.generateUpdateConfigProtobuff(channelId, originalConfigProto, updatedConfigProtobuff, configtxlatorURL);
    })
    .then((updateConfig) => {
        let signatures = [];
        signatures.push(ordererConnection.signChannelConfig(updateConfig));
        return ordererConnection.updateChannelConfig({channelId, updateConfigProtobuff: updateConfig, signatures});
    })
    .then((res) => {
        console.log(res);
    })
    .catch((e) => {console.log(e)});