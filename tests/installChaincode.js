let AdminConnection = require('../src/AdminMSPConnection');


let chaincodeId = "hello-sdk";
let chaincodeVersion = "0.1.0";
let chaincodePath = "optherium.com/hello";
let orgName = "Org1";
let userName = "admin-msp";
let tls = true;
let userCredentials = require(`./${userName}-${orgName.toLowerCase()}Credentials.json`);
let connectionProfile = require(`./connectionProfile-${orgName.toLowerCase()}.json`);

let connection = new AdminConnection({connectionName: userName, orgName: "Org1", connectionProfile, credentials: userCredentials, tls});

Promise.resolve()
    .then(() => {
        return connection.init({});
    })
    .then(() => {
        return connection.installChaincodeOnPeers({chaincodeId, chaincodeVersion, chaincodePath, targets: ["peer0.org1.optherium.com"]});
    })
    .then((result) => {
        console.log(result);
    })
    .catch((e) => {console.log(e)});