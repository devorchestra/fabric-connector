let AdminConnection = require('../src/AdminMSPConnection');
let TxObserver = require('../src/TxObserver');

let channelId = "test-sdk-channel";
let chaincodeId = "hello-sdk";
let chaincodeVersion = "0.1.0";
let orgName = "Org1";
let userName = "admin-msp";
let tls = true;
let userCredentials = require(`./${userName}-${orgName.toLowerCase()}Credentials.json`);
let connectionProfile = require(`./connectionProfile-${orgName.toLowerCase()}.json`);

let endorsementPolicy = {
    identities: [
        {role: {name: 'member', mspId: 'Org1MSP'}},
        {role: {name: 'member', mspId: 'Org2MSP'}}
    ],
    policy: {
        '2-of': [
            {'signed-by': 0},
            {'signed-by': 1}
        ]
    }
};

let txObserver = new TxObserver({connectionName: userName, orgName, connectionProfile, credentials: userCredentials, tls});
let connection = new AdminConnection({connectionName: userName, orgName, connectionProfile, credentials: userCredentials, tls});

txObserver.startObserver()
    .then(() => {
        return connection.init({txObserverInstance: txObserver});
    })
    .then(() => {
        return connection.instantiateChaincodeOnChannel({channelId, chaincodeId, chaincodeVersion, endorsementPolicy, target:"peer0.org1.optherium.com"})
    })
    .then((result) => {
        console.log(result);
    })
    .catch((e) => {console.log(e)});