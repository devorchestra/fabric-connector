let OuterClientConnection = require('../src/OuterClientConnection');
let ServiceDiscoveryProvider = require('../src/serviceDiscovery/ServiceDiscoveryProvider');
let TxObserver = require('../src/TxObserver');


let channelId = "admin-msp-channel-1";
let orgName = "Org1";
let userName = "admin-msp";
let tls = true;
let userCredentials = require(`./${userName}-${orgName.toLowerCase()}Credentials.json`);
let connectionProfile = require(`./connectionProfile-${orgName.toLowerCase()}.json`);

let txObserver = new TxObserver({connectionName: userName, orgName, connectionProfile, credentials: userCredentials, tls});
let connection = new OuterClientConnection({connectionName: userName, orgName, connectionProfile, credentials: userCredentials, tlsClientCredentials: userCredentials});
let serviceDiscoveryProvider = new ServiceDiscoveryProvider({connectionName: userName, orgName, connectionProfile, credentials: userCredentials, tls});


let proposal, txId;

txObserver.startObserver()
    .then(() => {
        return serviceDiscoveryProvider.start();
    })
    .then(() => {
        return connection.init({txObserverInstance: txObserver, serviceDiscoveryProvider: serviceDiscoveryProvider})
    })
    .then(() => {
        let answ = connection.generateUnsignedProposal({
            clientCertificate: userCredentials.certificate,
            channelId,
            chaincodeId: "hello",
            fcn: "createCar",
            args: ["blue", "BMW", "2118"]
        });
        proposal = answ.proposal;
        txId = answ.txId;
        let signedProposal = _signProposal(proposal.toBuffer());
        return connection.sendSignedProposal({channelId, chaincodeId: "hello", signedProposal})
    })
    .then((proposalResponses) => {
        const commitReq = {
            proposalResponses,
            proposal,
            channelId,
        };
        const transactionRequest = connection.generateUnsignedTransaction(commitReq);
        const signedTransactionRequest = _signProposal(transactionRequest.toBuffer());

        return connection.sendSignedTransaction({
            channelId,
            signedTransactionRequest,
            transactionRequest,
            txId
        })
    })
    .then((data) => {
        console.log(data);
    })
    .catch((data) => {
        console.log(data);
    });

/**
 * Auxiliary
 */

const elliptic = require('elliptic');
const jsrsa = require('jsrsasign');
const crypto = require('crypto');


function _signProposal(proposalBytes) {
    const signature = _sign(userCredentials.privateKey, proposalBytes, 'sha2', 256);
    const signedProposal = {signature, proposal_bytes: proposalBytes};
    return signedProposal;
}

function _preventMalleability(sig, curveParams) {
    const ordersForCurve = {
        'secp256r1': {
            'halfOrder': elliptic.curves['p256'].n.shrn(1),
            'order': elliptic.curves['p256'].n
        },
        'secp384r1': {
            'halfOrder': elliptic.curves['p384'].n.shrn(1),
            'order': elliptic.curves['p384'].n
        }
    };
    const halfOrder = ordersForCurve[curveParams.name]['halfOrder'];
    if (!halfOrder) {
        throw new Error('Can not find the half order needed to calculate "s" value for immalleable signatures. Unsupported curve name: ' + curveParams.name);
    }

    // in order to guarantee 's' falls in the lower range of the order, as explained in the above link,
    // first see if 's' is larger than half of the order, if so, it needs to be specially treated
    if (sig.s.cmp(halfOrder) === 1) { // module 'bn.js', file lib/bn.js, method cmp()
        // convert from BigInteger used by jsrsasign Key objects and bn.js used by elliptic Signature objects
        const bigNum = ordersForCurve[curveParams.name]['order'];
        sig.s = bigNum.sub(sig.s);
    }

    return sig;
}

function _sign(privateKey, proposalBytes, algorithm, keySize) {
    const EC = elliptic.ec;
    const {KEYUTIL} = jsrsa;
    const hashAlgorithm = algorithm.toUpperCase();
    const ecdsaCurve = elliptic.curves[`p${keySize}`];
    const ecdsa = new EC(ecdsaCurve);
    const key = KEYUTIL.getKey(privateKey);

    const signKey = ecdsa.keyFromPrivate(key.prvKeyHex, 'hex');

    let sha256 = crypto.createHash('sha256')
    sha256.update(proposalBytes);
    let digest = sha256.digest('hex');

    let sig = ecdsa.sign(Buffer.from(digest, 'hex'), signKey);
    sig = _preventMalleability(sig, key.ecparams);

    return Buffer.from(sig.toDER());
}
