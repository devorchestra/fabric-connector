let AdminMSPConnection = require('../src/AdminMSPConnection');


let channelId = "admin-msp-channel-1";
let orgName = "Org1";
let userName = "admin-msp";
let tls = true;
let userCredentials = require(`./${userName}-${orgName.toLowerCase()}Credentials.json`);
let connectionProfile = require(`./connectionProfile-${orgName.toLowerCase()}.json`);

let connection = new AdminMSPConnection({connectionName: userName, orgName, connectionProfile, credentials: userCredentials, tls});

Promise.resolve()
    .then(() => {
        return connection.init()
    })
    .then(() => {
        return connection.joinChannel(channelId);
    })
    .then((result) => { console.log(result) })
    .catch((e) => {console.log(e)});

