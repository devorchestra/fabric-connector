let AdminConnection = require('../src/AdminCAConnection');
let fs = require('fs');


let orgName = "Org1";
let userName = "admin-ca";
let tls = true;
let connectionProfile = require(`./connectionProfile-${orgName.toLowerCase()}.json`);
let caName = connectionProfile.organizations[orgName].certificateAuthorities[0];

let connection = new AdminConnection({connectionName: userName, caName, orgName, connectionProfile, tls});

connection.initWithoutCredentials()
    .then(() => {
        return connection.enroll({enrollmentID: "admin", enrollmentSecret: "adminpw"})
    })
    .then((data) => {
        fs.writeFileSync(`./${userName}-${orgName.toLowerCase()}Credentials.json`, JSON.stringify(data));
        console.log(data);
    })
    .catch((error) => {
        console.log(error);
    });
