let AdminConnection = require('../src/AdminMSPConnection');


let mspAdmin = "admin-msp";
let configtxlatorURL = 'http://127.0.0.1:7059';
let channelId = "admin-msp-channel-1";
let orgName = "Org1";
let tls = true;
let mspAdminCredentials = require(`./${mspAdmin}-${orgName.toLowerCase()}Credentials.json`);
let connectionProfile = require(`./connectionProfile-${orgName.toLowerCase()}.json`);

let anchorPeers = [
    {"host": "peer0.org1.optherium.com","port": 7051},
    {"host": "peer1.org1.optherium.com","port": 7151}
    ];

let connection = new AdminConnection({connectionName: mspAdmin, orgName, connectionProfile, credentials: mspAdminCredentials, tls, configtxlatorURL});

Promise.resolve()
    .then(() => {
        return Promise.all([
            connection.init(),
        ])
    })
    .then(() => {
        return connection.addAnchorPeers({channelId, anchorPeers});
    })
    .then((data) => {
        console.log(data);
    });
