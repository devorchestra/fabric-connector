let AdminConnection = require('../src/AdminCAConnection');
let fs = require('fs');

let connectionProfile = require('./connectionProfile.json');

let userName = "admin-ca";
let adminCaCredentials = require(`./${userName}Credentials.json`);
let tls = true;

let connection = new AdminConnection({
    connectionName: userName,
    caName: "ca-int.org1.optherium.com",
    orgName: "Org1",
    connectionProfile,
    tls,
    credentials: adminCaCredentials
});

connection.init()
    .then(() => {
        return connection.listAffiliations();
    })
    .then((data) => {
        console.log(data);
    });