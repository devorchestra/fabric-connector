let AdminConnection = require('../src/AdminMSPConnection');
let ServiceDiscoveryProvider = require('../src/serviceDiscovery/ServiceDiscoveryProvider');


let configtxlatorURL = 'http://127.0.0.1:7059';
let channelConfig = require(`./channelCreationConfig.json`);
let channelId = "admin-msp-channel-1";
let orgName = "Org1";
let userName = "admin-msp";
let tls = true;
let userCredentials = require(`./${userName}-${orgName.toLowerCase()}Credentials.json`);
let connectionProfile = require(`./connectionProfile-${orgName.toLowerCase()}.json`);

channelConfig.channel_id = channelId;

let connection = new AdminConnection({connectionName: userName, orgName, connectionProfile, credentials: userCredentials, tls, configtxlatorURL});
let serviceDiscoveryProvider = new ServiceDiscoveryProvider({connectionName: userName, orgName, connectionProfile, credentials: userCredentials, tls});

serviceDiscoveryProvider.start()
    .then(() => {
        return connection.init({serviceDiscoveryProvider})
    })
    .then(() => {

        return connection.createChannel(channelId, channelConfig);
    })
    .then((result) => { console.log(result) })
    .catch((e) => {console.log(e)});


