let AdminConnection = require('../src/AdminMSPConnection');


let tls = true;
let ordererAdmin = "orderer-admin";
let channelId = "orderer-system-channel";
let configtxlatorURL = 'http://127.0.0.1:7059';
let ordererAdminCredentials = require(`./${ordererAdmin}Credentials.json`);
let connectionProfile = require('./connectionProfile-org1.json');



let ordererConnection = new AdminConnection({
    connectionName: ordererAdmin,
    orgName: "Orderer",
    connectionProfile,
    credentials: ordererAdminCredentials,
    tls,
    configtxlatorURL
});

Promise.resolve()
    .then(() => {
        return Promise.all([
            ordererConnection.init()
        ])
    })
    .then(() => {

        return ordererConnection.getGenesisBlock(channelId, configtxlatorURL)
    })
    .then((block) => {
        console.log(block);
    });