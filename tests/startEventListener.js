let ChannelListenerConnection = require('../src/ChannelListenerConnection');


let channelId = "test-sdk-channel";
let orgName = "Org1";
let userName = "admin-msp";
let tls = true;
let userCredentials = require(`./${userName}-${orgName.toLowerCase()}Credentials.json`);
let connectionProfile = require(`./connectionProfile-${orgName.toLowerCase()}.json`);


let connection = new ChannelListenerConnection({connectionName: userName, orgName, connectionProfile, channelId, peerName: "peer0.org1.optherium.com", credentials: userCredentials, tls});

connection.establishConnection();

connection.on('block', (data) => {console.log(data)});
