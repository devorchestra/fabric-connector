let AdminMSPConnection = require('../src/AdminMSPConnection');
let fs = require('fs');

let channelId = "admin-msp-channel-1";
let channelTx = fs.readFileSync(`./${channelId}.tx`);
let orgName = "Org1";
let userName = "admin-msp";
let tls = true;
let userCredentials = require(`./${userName}-${orgName.toLowerCase()}Credentials.json`);
let connectionProfile = require(`./connectionProfile-${orgName.toLowerCase()}.json`);

let connection = new AdminMSPConnection({connectionName: userName, orgName, connectionProfile, credentials: userCredentials, tls});

Promise.resolve()
    .then(() => {
        return connection.init()
    })
    .then(() => {
        return connection.createChannelFromEnvelope(channelId, channelTx);
    })
    .then((result) => { console.log(result) })
    .catch((e) => {console.log(e)});