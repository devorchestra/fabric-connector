let ClientConnection = require('../src/ClientConnection');
let ServiceDiscoveryProvider = require('../src/serviceDiscovery/ServiceDiscoveryProvider');
let TxObserver = require('../src/TxObserver');


let channelId = "admin-msp-channel-1";
let orgName = "Org1";
let userName = "admin-msp";
let tls = true;
let userCredentials = require(`./${userName}-${orgName.toLowerCase()}Credentials.json`);
let connectionProfile = require(`./connectionProfile-${orgName.toLowerCase()}.json`);

let txObserver = new TxObserver({connectionName: userName, orgName, connectionProfile, credentials: userCredentials, tls});
let connection = new ClientConnection({connectionName: userName, orgName, connectionProfile, credentials: userCredentials, tls});
let serviceDiscoveryProvider = new ServiceDiscoveryProvider({connectionName: userName, orgName, connectionProfile, credentials: userCredentials, tls});


txObserver.startObserver()
    .then(() => {
        return serviceDiscoveryProvider.start();
    })
    .then(() => {
        return connection.init({txObserverInstance: txObserver, serviceDiscoveryProvider: serviceDiscoveryProvider})
    })
    .then(() => {
//        return connection.queryInChaincode({chaincodeId: "hello", fcn: "simpleQuery", args: ["blue", "BMW", "2118"], channelId})
        return connection.executeTxInChaincode({chaincodeId: "hello", fcn: "createCar", args: ["blue", "BMW", "2118"], channelId})
    })
    .then((data) => {
        console.log(data);
    })
    .catch((error) => {
        console.log(error);
    });