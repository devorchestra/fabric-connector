let AdminConnection = require('../src/AdminCAConnection');
let ClientConnection = require('../src/ClientConnection');


let adminName = "admin-ca";
let orgName = "Org1";
let tls = true;
let adminCredentials = require(`./${adminName}-${orgName.toLowerCase()}Credentials.json`);
let connectionProfile = require(`./connectionProfile-${orgName.toLowerCase()}.json`);
let caName = connectionProfile.organizations[orgName].certificateAuthorities[0];


let enrollmentID = "user-1-org1";
let enrollmentSecret = "userpw";

let connection = new AdminConnection({connectionName: adminName, caName, orgName, connectionProfile, credentials: adminCredentials, tls});

connection.init()
    .then(() => {
        return connection.registerClientIdentity({enrollmentID, enrollmentSecret, maxEnrollments: 1, affiliation: "org1.departament1", arrts: [{name: "participandId", value: "user-1", ecert: true}]})
    })
    .then(() => {
        connection = new ClientConnection({connectionName: enrollmentID, caName, orgName, connectionProfile});
        return connection.initWithoutCredentials()
    })
    .then(() => {
        return connection.enroll({enrollmentID, enrollmentSecret})
    })
    .then((data) => {
        console.log(data);
    })
    .catch((error) => {
        console.log(error);
    })