# fabric-connector
A wrapper for hyperledger fabric-sdk modules:

1. https://www.npmjs.com/package/fabric-client

2. https://www.npmjs.com/package/fabric-ca-client

Main goal of this module is to provide for developers a more convenient (in other words, a higher level of abstraction) tools and function to interact with Hyperledger Fabric network.

## Features
    * Advanced logging
    * Fault tolerante Event Hub
    * Easy support of TLS
    * High availability queries to peers
    * Improved Service Discovery
    * Convenient tools for administration

## How to use
See index.d.ts types file

## Documentation
You can use `typedoc` to generate documentation of the module:
```
$ typedoc --out docs/ --includeDeclarations index.d.ts
```