let Promise = require('bluebird');
let _ = require('lodash');


let BaseFabricConnection = require("./BaseFabricConnection");
let logger = require('./logger');
let InternalError = require('./InternalError');


class OuterClientConnection extends BaseFabricConnection {

    constructor(options) {
        options.tls = false;
        if (options.tlsClientCredentials) {
            options.tls = true;
        }
        super(options);
        if (options.tlsClientCredentials) {
            this.tlsClientCredentials = options.tlsClientCredentials;
        }
        this.serviceDiscoveryProvider = null;
        this.txObserver = null;
        this.discoveryChannels = null;
    }

    init(options) {
        return super.initWithoutCredentials()
            .then(() => {
                if (options && options.txObserverInstance) {
                    logger.info(`TxObserver instance is provided for OuterClientConnection connection`);
                    this.txObserver = options.txObserverInstance;

                }
                if (options && options.serviceDiscoveryProvider) {
                    logger.info(`ServiceDiscoveryProvider instance is provided for OuterClientConnection connection`);
                    this.serviceDiscoveryProvider = options.serviceDiscoveryProvider;
                    this.discoveryChannels = new Map();
                }

                if (this.tls) {
                    if (!this.tlsClientCredentials) {
                        throw new Error("Tls client credentials are not provided");
                    }
                    this.fabricClient.setTlsClientCertAndKey(this.tlsClientCredentials.certificate, this.tlsClientCredentials.privateKey);
                }


            })
    }

    _getChannel(channelId) {

        if (!this.serviceDiscoveryProvider) {
            return this.fabricClient.getChannel(channelId);
        }

        let channel = this.discoveryChannels.get(channelId);
        if (!channel) {
            // need to do some tricks with instance of fabric-sdk Channel class
            // this connection with channel will use service discovery results provided by ServiceDiscoveryProvider instance
            channel = this.fabricClient.getChannel(channelId);
            channel._endorsement_handler = this.serviceDiscoveryProvider.getEndorsementHandler(channel);
            channel._commit_handler = this.serviceDiscoveryProvider.getCommitHandler(channel);
            channel._use_discovery = true;
            channel._channel_peers = this.serviceDiscoveryProvider.getChannelPeers(channelId);
            channel._orderers = this.serviceDiscoveryProvider.getChannelOrderers(channelId);
            channel.sendSignedProposalWithDiscovery = this.serviceDiscoveryProvider.buildSendSignedProposalWithDiscoveryFunction();
            this.discoveryChannels.set(channelId, channel);
        }

        return channel;
    }

     generateUnsignedProposal(req) {
        let  mspId = this.connectionProfile.organizations[this.orgName].mspid;
        let { proposal, txId } = this._getChannel(req.channelId).generateUnsignedProposal(req, mspId, req.clientCertificate, false);
        return {proposal, txId: txId.getTransactionID()}

    }

    sendSignedProposal({channelId, chaincodeId, signedProposal, targets}) {

        return Promise.resolve()
            .then(() => {
                // with Service Discovery
                if (this.serviceDiscoveryProvider) {
                    return this._getChannel(channelId).sendSignedProposalWithDiscovery(signedProposal, chaincodeId, this.timeouts.peerRequest);
                }
                // without
                let peersToSendProposal = [];
                if (!targets) {
                    let peerName =  Object.keys(this.connectionProfile.channels[channelId].peers)[0];
                    peersToSendProposal.push(this.fabricClient.getPeer(peerName));
                } else {
                    for (let peerName of targets) {
                        peersToSendProposal.push(this.fabricClient.getPeer(peerName));
                    }
                }

                let req = {signedProposal, targets: peersToSendProposal};
                return this._getChannel(channelId).sendSignedProposal(req, this.timeouts.peerRequest);
            })
            .then((proposalResponses) => {
                if (!proposalResponses || !proposalResponses[0]) {
                    throw new InternalError('No valid response')
                }
                for (let proposalResponse of proposalResponses) {
                    if (proposalResponse instanceof Error) {
                        throw proposalResponse;
                    }
                    if (!proposalResponse.response || proposalResponse.response.status !== 200) {
                        let msg =  proposalResponse.response ? proposalResponse.response.message : "";
                        throw new InternalError(`One of the peer response is null or status is not 200: ${msg}`);                    }
                }
                return proposalResponses;
            });
    }

    queryWithSignedProposal(req) {
        let channel;
        return Promise.resolve()
            .then(() => {
                channel = this._getChannel(req.channelId);

                if (!req.target) {
                    let targets = [];
                    for (let peerName of Object.keys(this.connectionProfile.channels[req.channelId].peers)) {
                        let peerInfo = this.connectionProfile.channels[req.channelId].peers[peerName];
                        if (peerInfo.chaincodeQuery) {
                            targets.push(peerName);
                        }
                    }
                    if (targets.length === 0) {
                        throw new InternalError(`Connection profile doesn't have peer with "chaincodeQuery" property for channel ${req.channelId}`);
                    }
                    req.targets = [];
                    req.targets.push(this.fabricClient.getPeer(_.sample(targets)));
                } else {
                    req.targets = [this.fabricClient.getPeer(req.target)];
                }
            })
            .then(() => {
                return channel.sendSignedProposal(req, this.timeouts.peerRequest);
            })
            .then((proposalResponses) => {
                if (!proposalResponses || !proposalResponses[0]) {
                    throw new InternalError('No valid response');
                }
                for (let proposalResponse of proposalResponses) {
                    if (proposalResponse instanceof Error) {
                        throw proposalResponse;
                    }
                }
                let data = {status: proposalResponses[0].response.status, message: proposalResponses[0].response.message};
                if (proposalResponses[0].response.payload instanceof Buffer) {
                    let payload = proposalResponses[0].response.payload.toString();
                    try {
                        payload = JSON.parse(payload);
                    } catch (e) {}
                    data.payload = payload
                }
                return data;
            });
    }

    generateUnsignedTransaction({channelId, proposal, proposalResponses}) {
        proposal.getHeader = function () {
            return this.header;
        };
        return this.fabricClient.getChannel(channelId).generateUnsignedTransaction({proposal, proposalResponses});
    }

    sendSignedTransaction({channelId, signedTransactionRequest, transactionRequest, txId, orderer}) {

        if (!this.txObserver) {
            return Promise.reject(new Error(`TxObserver instance is not  provided for ${this.name} connection`));
        }

        let requestToOrderer = {signedProposal: signedTransactionRequest, request: transactionRequest};
        if (!this.serviceDiscoveryProvider) {
            //nothing, orderer has to be undefined
        } else if (!orderer) {
            requestToOrderer.orderer = this.connectionProfile.channels[channelId].orderers[0];
        } else {
            requestToOrderer.orderer = orderer;
        }

        return new Promise((resolve, reject) => {
            // Orderer
            this._getChannel(channelId).sendSignedTransaction(requestToOrderer, this.timeouts.ordererBroadcast)
                .then((ordererResult) => {
                    if (!ordererResult || ordererResult.status !== 'SUCCESS') {
                        reject(new InternalError('Failed to send transaction to Orderer'));
                        this.txObserver.stopListenToTx(channelId, txId);
                    }
                })
                .catch(reject);

            // Tx listnerer
            this.txObserver.listenToTx(channelId, txId, this.timeouts.txCommitment)
                .then(resolve)
                .catch(reject);
        });
    }
}

module.exports = OuterClientConnection;