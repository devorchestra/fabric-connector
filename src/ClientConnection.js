let Promise = require('bluebird');
let _ = require('lodash');

let BaseFabricConnection = require("./BaseFabricConnection");
let logger = require('./logger');
let InternalError = require('./InternalError');


class ClientConnection extends BaseFabricConnection {

    constructor(options) {
        super(options);
        this.txObserver = null;
        this.serviceDiscoveryProvider = null;
        this.discoveryChannels = null;
    }

    init(options) {
        return Promise.resolve()
            .then(() => {
                if (options && options.txObserverInstance) {
                    logger.info(`TxObserver instance is provided for ${this.name} connection`);
                    this.txObserver = options.txObserverInstance;

                }
                if (options && options.serviceDiscoveryProvider) {
                    logger.info(`ServiceDiscoveryProvider instance is provided for ${this.name} connection`);
                    this.serviceDiscoveryProvider = options.serviceDiscoveryProvider;
                    this.discoveryChannels = new Map();
                }
                return super.init();
            })
    }

    _getChannel(channelId) {

        if (!this.serviceDiscoveryProvider) {
            return this.fabricClient.getChannel(channelId);
        }

        let channel = this.discoveryChannels.get(channelId);
        if (!channel) {
            // need to do some tricks with instance of fabric-sdk Channel class
            // this connection with channel will use service discovery results provided by ServiceDiscoveryProvider instance
            channel = this.fabricClient.getChannel(channelId);
            channel._endorsement_handler = this.serviceDiscoveryProvider.getEndorsementHandler(channel);
            channel._commit_handler = this.serviceDiscoveryProvider.getCommitHandler(channel);
            channel._use_discovery = true;
            channel._channel_peers = this.serviceDiscoveryProvider.getChannelPeers(channelId);
            channel._orderers = this.serviceDiscoveryProvider.getChannelOrderers(channelId);
            this.discoveryChannels.set(channelId, channel);
        }

        return channel;
    }

    executeTxInChaincode(req) {
        let chaincodeResponse;

        if (!this.txObserver) {
            return Promise.reject(new Error(`TxObserver instance is not  provided for ${this.name} connection`));
        }

        return Promise.resolve()
            .then(() => {
                if (this.serviceDiscoveryProvider) {
                    req.targets = null;
                    req.orderer = null;
                } else if (!req.targets || req.targets.length === 0) {
                    let peerName = Object.keys(this.connectionProfile.channels[req.channelId].peers)[0];
                    req.targets = [peerName];
                } else if (!req.orderer) {
                    req.orderer = this.connectionProfile.channels[req.channelId].orderers[0];
                }
                req.txId = this.fabricClient.newTransactionID();
                return this._sendTxProposal(req);
            })
            .then((results) => {
                let proposalResponses = results[0];
                let proposal = results[1];
                if (!proposalResponses || !proposalResponses[0]) {
                    throw new InternalError('No valid response')
                }
                for (let proposalResponse of proposalResponses) {
                    if (proposalResponse instanceof Error) {
                        throw proposalResponse;
                    }
                    if (!proposalResponse.response || proposalResponse.response.status !== 200) {
                        let msg =  proposalResponse.response ? proposalResponse.response.message : "";
                        throw new InternalError(`One of the peer response is null or status is not 200: ${msg}`);
                    }
                }

                chaincodeResponse = {
                    status: proposalResponses[0].response.status,
                    message: proposalResponses[0].response.message,
                };
                if (proposalResponses[0].response.payload instanceof Buffer) {
                    let payload = proposalResponses[0].response.payload.toString();
                    try {
                        payload = JSON.parse(payload);
                    } catch (e) {}
                    chaincodeResponse.payload = payload;
                }
                return this._sendTxToOrderer(req, proposal, proposalResponses);
            })
            .then((eventHubResult) => {

                if(!eventHubResult || eventHubResult.code !== 'VALID') {
                    throw new InternalError('Transaction failed to be committed to the ledger due to :: '+ eventHubResult.code);
                }
                return chaincodeResponse;
            })
    }

    _sendTxProposal(request) {
        request.chainId = request.channelId;
        return this._getChannel(request.channelId).sendTransactionProposal(request, this.timeouts.peerRequest)
    }

    _sendTxToOrderer(request, proposal, proposalResponses) {

        let txIdString = request.txId.getTransactionID();
        return new Promise((resolve, reject) => {

            let ordererRequest = {proposal, proposalResponses};
            if (!this.serviceDiscoveryProvider) {
                ordererRequest.orderer = request.orderer;
            }
            // Orderer
            this._getChannel(request.channelId).sendTransaction(ordererRequest, this.timeouts.ordererBroadcast)
                .then((ordererResult) => {
                    if (!ordererResult || ordererResult.status !== 'SUCCESS') {
                        reject(new InternalError('Failed to send transaction to Orderer'));
                        this.txObserver.stopListenToTx(request.channelId, txIdString);
                    }
                })
                .catch(reject);

            // Tx listnerer
            this.txObserver.listenToTx(request.channelId, txIdString, this.timeouts.txCommitment)
                .then(resolve)
                .catch(reject);
        });
    }

    queryInChaincode(req) {
        let channel;
        return Promise.resolve()
            .then(() => {
                channel = this._getChannel(req.channelId);
                req.chainId = req.channelId;
                req.request_timeout = this.timeouts.peerRequest;
                if (!req.target) {
                    let targets = [];
                    for (let peerName of Object.keys(this.connectionProfile.channels[req.channelId].peers)) {
                        let peerInfo = this.connectionProfile.channels[req.channelId].peers[peerName];
                        if (peerInfo.chaincodeQuery) {
                            targets.push(peerName);
                        }
                    }
                    if (targets.length === 0) {
                        throw new InternalError(`Connection profile doesn't have any peer with "chaincodeQuery" property for channel ${req.channelId}`);
                    }
                    req.targets = [];
                    req.targets.push(_.sample(targets));
                } else {
                    req.targets = [req.target];
                }
            })
            .then(() => {
                return channel.queryByChaincode(req);
            })
            .then((responses) => {
                if (!responses || responses.length === 0) {
                    throw new InternalError('No valid response');
                }
                for (let proposalResponse of responses) {
                    if (proposalResponse instanceof Error) {
                        throw proposalResponse;
                    }
                }
                let data = {status: 200};
                if (responses[0] instanceof Buffer) {
                    let payload = responses[0].toString();
                    try {
                        payload = JSON.parse(payload);
                    } catch (e) {}
                    data.payload = payload
                }
                return data;
            });
    }
}

module.exports = ClientConnection;