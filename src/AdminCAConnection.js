let Promise = require('bluebird');

let BaseFabricConnection = require("./BaseFabricConnection");
let logger = require('./logger');

// All CA methods (except enroll) uses timeout from library config `connection-timeout`
class AdminCAConnection extends BaseFabricConnection {

    constructor(options) {
        super(options);
    }

    registerClientIdentity(clientData) {
        return Promise.resolve()
            .then(() => {
                return this.fabricClient.getUserContext(this.name, true);
            })
            .then((adminUser) => {
                return this.fabricCaClient.register(clientData, adminUser);
            })
            .then((secret) => {
                return {enrollmentID: clientData.enrollmentID, enrollmentSecret: secret}
            })

    }

    listAffiliations() {
        return Promise.resolve()
            .then(() => {
                return this.fabricClient.getUserContext(this.name, true);
            })
            .then((adminUser) => {
                return this.fabricCaClient.newAffiliationService().getAll(adminUser);
            })
            .then((res) => {
                if (res.success === true) {
                    return res.result.affiliations;
                }

                let errorMsg = 'Error occurred during listing affiliations';
                if (res.errors[0]) {
                    errorMsg = res.errors[0].message;
                }
                throw new Error(errorMsg);
            });
    }

    addAffiliation(request) {
        return Promise.resolve()
            .then(() => {
                return this.fabricClient.getUserContext(this.name, true);
            })
            .then((adminUser) => {
                request.force = true;
                return this.fabricCaClient.newAffiliationService().create(request, adminUser);
            })
            .then((res) => {
                if (res.success === true) {
                    return res.result;
                }
                let errorMsg = 'Error occurred during listing affiliations';
                if (res.errors[0]) {
                    errorMsg = res.errors[0].message;
                }
                throw new Error(errorMsg);
            });
    }

    deleteAffiliation(request) {
        return Promise.resolve()
            .then(() => {
                return this.fabricClient.getUserContext(this.name, true);
            })
            .then((adminUser) => {
                return this.fabricCaClient.newAffiliationService().delete(request, adminUser);
            })
            .then((res) => {
                if (res.success === true) {
                    return res.result;
                }
                let errorMsg = 'Error occurred during listing affiliations';
                if (res.errors[0]) {
                    errorMsg = res.errors[0].message;
                }
                throw new Error(errorMsg);
            });
    }
}

module.exports = AdminCAConnection;