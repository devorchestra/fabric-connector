let createLogger = require('devorchestra-logger');
let packageJson = require('../package.json');


let logger = createLogger(packageJson.name, process.env.LOG_LEVEL);
module.exports = logger;