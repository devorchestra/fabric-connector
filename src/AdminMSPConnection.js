let Promise = require('bluebird');


let ClientConnection = require("./ClientConnection");
let Configtxlator = require('./Configtxlator');
let logger = require('./logger');
let InternalError = require('./InternalError');


class AdminMSPConnection extends ClientConnection {

    constructor(options) {
        super(options);
        if (options.configtxlatorURL) {
            this.configtxlator = new Configtxlator(options.configtxlatorURL);
        }
    }

    buildSignableChannelConfig(configEnvelope) {
        return this.fabricClient.extractChannelConfig(configEnvelope);
    }

    signChannelConfig(config) {
        return this.fabricClient.signChannelConfig(config)
    }

    _createChannelWithProtobuff(channelId, config, signatures, orderer) {
        let txId = this.fabricClient.newTransactionID();

        // Channel doesn't exit, so can't use service discovery
        if (!orderer) {
            let orderers = Object.keys(this.connectionProfile.orderers);
            let number = Math.floor(Math.random()*orderers.length);
            orderer = orderers[number];
        }
        // TODO this method doesn't support custom timeout (broadcast to orderer), uses timeout from library config `request-timeout`
        return this.fabricClient.createChannel({name: channelId, config, txId, signatures, orderer})
            .then((result) => {
                if (result && result.status === 'SUCCESS') {
                    return result;
                } else {
                    let error = new Error(`Failed to create a channel`);
                    error.status = result.status;
                    error.info = result.info;
                    throw error;
                }
            });
    }

    createChannelFromEnvelope(channelId, configEnvelope, orderer) {
        let signableConfig = this.buildSignableChannelConfig(configEnvelope);
        let signature = this.signChannelConfig(signableConfig);
        return this._createChannelWithProtobuff(channelId, signableConfig, [signature], orderer);
    }

    createChannel(channelId, config, orderer) {

        if (!this.configtxlator) {
            return Promise.reject(new Error('Configtxlator is not defined!'));
        }

        return this.configtxlator.generateCreateChannelProtobuff(config)
            .then((configProtobuff) => {
                let signature = this.signChannelConfig(configProtobuff);
                return this._createChannelWithProtobuff(channelId, configProtobuff, [signature], orderer);
            });
    }

    getGenesisBlock(channelId) {
        let channel = this.fabricClient.getChannel(channelId);
        let txId = this.fabricClient.newTransactionID();

        if (!this.configtxlator) {
            return Promise.reject(new Error('Configtxlator is not defined!'));
        }
        // TODO this method doesn't support custom timeout (broadcast to orderer), uses timeout from library config `request-timeout`
        return channel.getGenesisBlock({txId})
            .then((block) => {
                return this.configtxlator.decodeBlock(block);
            });
    }

    getChannelConfigProtobuff({channelId, target, fromOrderer}) {
        let channel = this.fabricClient.getChannel(channelId);
        if (!target) target = null;
        let request;
        if (fromOrderer === true) {
            // TODO this method doesn't support custom timeout (broadcast to orderer), uses timeout from library config `request-timeout`
            request =  channel.getChannelConfigFromOrderer();
        } else {
            request = channel.getChannelConfig(target, this.timeouts.peerRequest);
        }

        return request
            .then((configEnvelope) => {
                return configEnvelope.config.toBuffer();
            })
    }

    joinChannel(channelId, targets) {
        let channel = this.fabricClient.getChannel(channelId);
        if (!targets || targets.length < 1) {
            targets = Object.keys(this.connectionProfile.channels[channelId].peers);
        }

        let txId = this.fabricClient.newTransactionID();
        // TODO this method doesn't support custom timeout (broadcast to orderer), uses timeout from library config `request-timeout`
        return channel.getGenesisBlock({txId})
            .then((block) => {
                txId = this.fabricClient.newTransactionID();
                return channel.joinChannel({txId, block, peers: targets}, this.timeouts.peerRequest);
            });
    }

    updateChannelConfig({channelId, updateConfigProtobuff, signatures, orderer}) {
        let txId = this.fabricClient.newTransactionID();

        if (this.serviceDiscoveryProvider) {
            orderer = this.serviceDiscoveryProvider.getRandomOrderer(channelId);
        } else if (!orderer) {
            orderer = this.connectionProfile.channels[channelId].orderers[0];
        }

        let request = {
            config: updateConfigProtobuff,
            signatures: signatures,
            name: channelId,
            txId,
            orderer
        };
        // TODO this method doesn't support custom timeout (broadcast to orderer), uses timeout from library config `request-timeout`
        return this.fabricClient.updateChannel(request);
    }

    addAnchorPeers(channelId, anchorPeers, orderer) {
        let orgAnchorData = {
            mod_policy: "Admins",
            value: { anchor_peers: anchorPeers}
        };
        let orgMSP = this.connectionProfile.organizations[this.orgName].mspid;
        let originalConfigProto;

        if (!this.configtxlator) {
            return Promise.reject(new Error('Configtxlator is not defined!'));
        }

        return this.getChannelConfigProtobuff({channelId, fromOrderer: false})
            .then((configProtobuff) => {
                originalConfigProto = configProtobuff;
                return this.configtxlator.decodeConfigProtobuff(originalConfigProto);
            })
            .then((config) => {
                config.channel_group.groups.Application.groups[orgMSP].values["AnchorPeers"] = orgAnchorData;
                return this.configtxlator.encodeConfigToProtobuff(config);
            })
            .then((updatedConfigProtobuff) => {
                return this.configtxlator.generateUpdateConfigProtobuff(channelId, originalConfigProto, updatedConfigProtobuff);
            })
            .then((updateConfig) => {
                let signatures = [];
                signatures.push(this.signChannelConfig(updateConfig));
                return this.updateChannelConfig({channelId, updateConfigProtobuff: updateConfig, signatures, orderer});
            });
    }

    installChaincodeOnPeers({chaincodeId, chaincodeVersion, chaincodePath, targets}) {

        if (!targets || targets.length < 1) {
            targets = this.connectionProfile.organizations[this.orgName].peers;
        }
        let request = {
            targets: targets,
            chaincodePath,
            chaincodeId,
            chaincodeVersion,
            // TODO
            //channelNames: "",
            //chaincodePackage: ""
        };

        return this.fabricClient.installChaincode(request, this.timeouts.peerChaincodeInstall)
            .then(([responses]) => {
                return responses;
            })
    }

    instantiateChaincodeOnChannel(req) {
        return this._instantiateOrUpgradeChaincodeOnChannel(req, 'instantiate');
    }

    upgradeChaincodeOnChannel(req) {
        return this._instantiateOrUpgradeChaincodeOnChannel(req, 'upgrade');

    }

    _instantiateOrUpgradeChaincodeOnChannel({channelId, chaincodeId, chaincodeVersion, endorsementPolicy, target}, command) {

        if (!this.txObserver) {
            return Promise.reject(new Error(`TxObserver instance is not provided for ${this.name} connection`));
        }

        let channel = this.fabricClient.getChannel(channelId);
        let txId = this.fabricClient.newTransactionID();
        let targets = [];
        if (!target) {
            targets.push(Object.keys(this.connectionProfile.channels[channelId].peers)[0]);
        } else {
            targets.push(target)
        }

        let request = {
            chaincodeId,
            chaincodeVersion,
            fcn: "init",
            args: [],
            txId,
            'endorsement-policy': endorsementPolicy,
            targets
        };
        let action;
        if (command === 'instantiate') {
            action = 'sendInstantiateProposal';
        } else {
            action = 'sendUpgradeProposal';
        }

        return channel[action](request, this.timeouts.peerRequest)
            .then((results) => {
                let proposalResponses = results[0];
                let proposal = results[1];
                if (!proposalResponses || !proposalResponses[0]) {
                    throw new InternalError('No valid response')
                }
                if (proposalResponses[0] instanceof Error) {
                    throw proposalResponses[0]
                }
                if (!proposalResponses[0].response || proposalResponses[0].response.status !== 200) {
                    let msg = proposalResponses[0].response ? proposalResponses[0].response.message : "";
                    throw new InternalError(`One of the peer response is null or status is not 200: ${msg}`);
                }

                return this._sendTxToOrderer({channelId, txId}, proposal, proposalResponses);
            })
    }
}

module.exports = AdminMSPConnection;