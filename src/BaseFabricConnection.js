let FabricClient = require('fabric-client');
let Promise = require('bluebird');
let FabricCaClient = require('fabric-ca-client');
const EventEmitter = require('events');


let logger = require('./logger');
let MemoryKeyValueStore = require('./MemoryKeyValueStore');

class BaseFabricConnection extends EventEmitter {

    constructor({connectionName, caName, orgName, connectionProfile, credentials, tls, timeouts}) {
        super();
        this.name = connectionName;
        this.connectionProfile = connectionProfile;
        this.caName = caName;
        this.orgName = orgName;
        this.credentials = credentials;
        this.tls = tls || false;
        this.memoryKVStore = null;
        this.cryptoMemoryKVStore = null; // contains MemoryKeyValueStore instances under key `_store`
        this.fabricClient = null;
        this.fabricCaClient = null;
        this.timeouts = {};
        this.timeouts.peerRequest = (timeouts && timeouts.peerRequest) ? timeouts.peerRequest : 5000;
        this.timeouts.peerChaincodeInstall = (timeouts && timeouts.peerChaincodeInstall) ? timeouts.peerChaincodeInstall : 120000;
        this.timeouts.ordererBroadcast = (timeouts && timeouts.ordererBroadcast) ? timeouts.ordererBroadcast : 3000;
        this.timeouts.txCommitment = (timeouts && timeouts.txCommitment) ? timeouts.txCommitment : 15000;
    }

    init() {
        return this._init()
            .then(() => {
                if (!this.credentials) {
                    throw new Error(`Credentials for connection ${this.name} are not provided`);
                }
                return this._setClientContext(this.credentials);
            })
    }

    initWithoutCredentials() {
        return this._init();
    }

    _init() {
        return Promise.resolve()
            .then(() => {
                return new MemoryKeyValueStore()
            })
            .then((store) => {
                this.memoryKVStore = store;
                this.fabricClient = new FabricClient();
                this.fabricClient.setStateStore(this.memoryKVStore);
                this.cryptoMemoryKVStore = FabricClient.newCryptoKeyStore(MemoryKeyValueStore);
                this.cryptoSuite = FabricClient.newCryptoSuite();
                this.cryptoSuite.setCryptoKeyStore(this.cryptoMemoryKVStore);
                this.fabricClient.setCryptoSuite(this.cryptoSuite);
                this.fabricClient.loadFromConfig(this.connectionProfile);
                if (this.caName) {
                    this.fabricCaClient = this.fabricClient.getCertificateAuthority(this.caName);
                }
            })
    }


    _setClientContext(credentials) {
        let data = {
            name: this.name,
            mspid: this.connectionProfile.organizations[this.orgName].mspid,
            role: null,
            affiliation: "",
            enrollmentSecret: "",
            enrollment: {
                signingIdentity: this.name, //any string, correct signingIdentity will be generated automatically
                identity: {
                    certificate: credentials.certificate
                }
            }
        };
        return Promise.resolve()
            .then(() => {
                return this.cryptoMemoryKVStore._getKeyStore()
            })
            .then((store) => {
                store.setValue(`${this.name}-priv`, credentials.privateKey);
                let user = new FabricClient.User(this.name);
                user.setCryptoSuite(this.cryptoSuite);
                return user.fromString(JSON.stringify(data), false);
            })
            .then((user) => {
                return this.fabricClient.setUserContext(user);
            })
            .then(() => {
                if (this.tls) {
                    this.fabricClient.setTlsClientCertAndKey(credentials.certificate, credentials.privateKey);
                }
                return this.fabricClient.saveUserToStateStore();
            })
    }


    enroll(credentials) {
        return Promise.resolve()
            .then(() => {
                this.fabricClient.getUserContext(this.name, true);
            })
            .then((user) => {
                if (user && user.isEnrolled()) {
                    return;
                }
                let profile = null;
                if (this.tls) {
                    profile = 'tls';
                }
                // TODO this method doesn't support custom timeout.
                return this.fabricCaClient.enroll({enrollmentID: credentials.enrollmentID, enrollmentSecret: credentials.enrollmentSecret, profile, attr_reqs: credentials.attrReqs})
                    .then((enrollment) => {
                        let payload = {
                            username: this.name,
                            mspid: this.connectionProfile.organizations[this.orgName].mspid,
                            cryptoContent: { privateKeyPEM: enrollment.key.toBytes(), signedCertPEM: enrollment.certificate }
                        };
                        return this.fabricClient.createUser(payload);
                    })
                    .then((user) => {
                        return this.fabricClient.setUserContext(user);
                    })
            })
            .then(() => {
                return this.getCredentials();
            })
            .catch((error) => {
                logger.error(`Failed to enroll and persist client ${this.name}. Error: ${error.message}`);
                throw error;
            });
    }

    enrollWithCSR(credentials) {
        return Promise.resolve()
            .then(() => {
                this.fabricClient.getUserContext(this.name, true);
            })
            .then((user) => {
                if (user && user.isEnrolled()) {
                    return;
                }
                // TODO this method doesn't support custom timeout
                return this.fabricCaClient.fabricCAServices._fabricCAClient.enroll(credentials.enrollmentID, credentials.enrollmentSecret, credentials.csr, null, credentials.attrReqs)
            })
            .then((enrollment) => {
                return {
                    certificate: enrollment.enrollmentCert
                }
            })
            .catch((error) => {
                logger.error(`Failed to enroll and persist client ${this.name}. Error: ${error.message}`);
                throw error;
            });
    }

    getCredentials() {
        let credentials = {};
        let signingIdentity;
        return Promise.resolve()
            .then(() => {
                return this.fabricClient.getUserContext(this.name, true);
            })
            .then((user) => {
                let data = JSON.parse(user.toString());
                signingIdentity = data.enrollment.signingIdentity;
                credentials.certificate = data.enrollment.identity.certificate;
                return this.cryptoMemoryKVStore._getKeyStore()
            })
            .then((store) => {
                return store.getValue(`${signingIdentity}-priv`);
            })
            .then((privateKey) => {
                credentials.privateKey = privateKey;
                return credentials;
            })
    }

    static setRequestTimeout(timeout) {
        FabricClient.setConfigSetting('request-timeout', timeout);
    }

    static setCARequestTimeout(timeout) {
        FabricCaClient.setConfigSetting('connection-timeout', timeout);
    }
}

module.exports = BaseFabricConnection;