let _ = require('lodash');

let logger = require('./logger');
let BaseFabricConnection = require("./BaseFabricConnection");
let InternalError = require('./InternalError');

let MAX_RECONNECT_TRY = 8;

class ChannelListenerConnection extends BaseFabricConnection {

    constructor(options) {
        super(options);
        this.channelId = options.channelId;
        this.eventSourcePeers = [];
        this.channelEventHub = undefined;
        this.reconnectCounter = 0;
        this.disconnecting = false;
        this.timeouts.peerReconnection = (options.timeouts && options.timeouts.peerReconnection) ? options.timeouts.peerReconnection : 500;
    }

    establishConnection(options) {
        return Promise.resolve()
            .then(() => {
                return this.init();
            })
            .then(() => {
                for (let peerName of Object.keys(this.connectionProfile.channels[this.channelId].peers)) {
                    let peerInfo = this.connectionProfile.channels[this.channelId].peers[peerName];
                    if (peerInfo.eventSource) {
                        this.eventSourcePeers.push(peerName);
                    }
                }
                if (this.eventSourcePeers.length === 0) {
                    throw new InternalError(`Connection profile doesn't have any peer with "eventSource" property for channel ${this.channelId}`)
                }
                this._connectToPeer(options);
            });

    }

    _connectToPeer(options) {
        let connectionPeerName = _.sample(this.eventSourcePeers);
        this.channelEventHub = this.fabricClient.getChannel(this.channelId).newChannelEventHub(connectionPeerName);
        this.channelEventHub.registerBlockEvent(
            (newBlock) => {
                this.emit('block', newBlock);
            },

            (reason) => {
                logger.info(`Channel ${this.channelId} block listener has been disconnected from peer ${connectionPeerName}: ${reason}`);
                if (this.disconnecting || this.reconnectCounter >= MAX_RECONNECT_TRY) {
                    return this.emit('closed', {channelId: this.channelId, reason});
                }
                this.emit('disconnected', {channelId: this.channelId, reason, peerName: connectionPeerName});
                logger.info(`Trying to reconnect in ${this.timeouts.peerReconnection} ms`);
                setTimeout(() => {
                    this.reconnectCounter++;
                    this._connectToPeer(options);
                }, this.timeouts.peerReconnection);
            },

            options);

        let fullBlocks = false;
        if (options && options.fullBlocks) {
            fullBlocks = options.fullBlocks;
        }
        this.channelEventHub.connect(fullBlocks, (error) => {
            if (!error) {
                let msg = `Channel ${this.channelId} block listener subscribed to peer ${connectionPeerName}`;
                logger.info(msg);
                this.emit('connected', {channelId: this.channelId, peerName: connectionPeerName});
                this.reconnectCounter = 0;
            } else {
                logger.error(`Errors occurred during subscribing to channel ${this.channelId} via peer ${connectionPeerName}`);
            }
        });
    }

    static _parseChannelBlock(block) {
        let parsedBlock = {};
        parsedBlock.blockNumber = block.header.number;
        parsedBlock.txs = [];
        let rawTxData;
        for (let i = 0; i < block.data.data.length; i++) {
            rawTxData = block.data.data[i];
            let tx = {};
            tx.validationCode = block.metadata.metadata[2][i];
            tx.timestamp = rawTxData.payload.header.channel_header.timestamp;
            tx.channelId = rawTxData.payload.header.channel_header.channel_id;
            tx.txId = rawTxData.payload.header.channel_header.tx_id;
            tx.typeString = rawTxData.payload.header.channel_header.typeString;
            tx.chaincodeId = rawTxData.payload.data.actions[0].payload.chaincode_proposal_payload.input.chaincode_spec.chaincode_id.name;
            tx.chaincodeProposalPayload = rawTxData.payload.data.actions[0].payload.chaincode_proposal_payload.input.chaincode_spec.input.args;
            let nsRWSet = rawTxData.payload.data.actions[0].payload.action.proposal_response_payload.extension.results.ns_rwset;
            tx.writeSet = [];
            for (let item of nsRWSet) {
                if (item.namespace !== tx.chaincodeId) {
                    continue;
                }
                tx.writeSet = tx.writeSet.concat(item.rwset.writes);
            }
            tx.chaincodeResponse = rawTxData.payload.data.actions[0].payload.action.proposal_response_payload.extension.response;
            tx.chaincodeEvents = rawTxData.payload.data.actions[0].payload.action.proposal_response_payload.extension.events;
            parsedBlock.txs.push(tx);
        }
        return parsedBlock;
    }

    disconnect() {
        this.disconnecting = true;
        this.channelEventHub.disconnect();
    }

}

module.exports = ChannelListenerConnection;