

'use strict';
let _ = require('lodash');

const logger = require('../logger');


/**
 * This is an implementation of the [CommitHandler]{@link module:api.CommitHandler} API.
 * It will submit transactions to be committed to one orderer at time from a provided
 * list or a list currently assigned to the channel.
 *
 * @class
 * @extends module:api.CommitHandler
 */
//TODO removed extends
class DiscoveryCommitHandler {

    /**
     * constructor
     *
     * @param {Channel} channel - The channel for this handler.
     */
    // TODO added serviceDiscoveryProvider
    constructor(channel, serviceDiscoveryProvider) {
        this._channel = channel;
        this.serviceDiscoveryProvider = serviceDiscoveryProvider;
    }

    /**
     * Factory method to create an instance of a committer handler.
     *
     * @param {Channel} channel - the channel instance that this commit
     *        handler will be servicing.
     * @returns {DiscoveryCommitHandler} The instance of the handler
     */
    static create(channel, serviceDiscoveryProvider) {
        return new DiscoveryCommitHandler(channel, serviceDiscoveryProvider);
    }

    initialize() {
        logger.debug('initialize - start');
    }

    async commit(params) {
        const method = 'commit';
        logger.debug('%s - start', method);


        let errorMsg = null;
        if (!params) {
            errorMsg = 'Missing all required input parameters';
        } else if (!params.request) {
            errorMsg = 'Missing "request" input parameter';
        } else if (!params.signed_envelope) {
            errorMsg = 'Missing "signed_envelope" input parameter';
        }

        if (errorMsg) {
            logger.error('Commit Handler error:' + errorMsg);
            throw new Error(errorMsg);
        }

        const request = Object.assign({}, params.request);

        // TODO changed here, timeout can be undefined cause Orderer instance uses default timeout from config
        let timeout = undefined;
        if (params.timeout) {
            timeout = params.timeout;
        }
        //done

        if (!request.orderer) {
            logger.debug('%s - using commit handler', method);
            // this will check the age of the results and get new results if needed
            try {
                // TODO changed to use serviceDiscoveryProvider
                await this.serviceDiscoveryProvider.getDiscoveryResults(this._channel.getName()); // this will cause a refresh if using discovery and the results are old
            } catch (error) {
                // No problem, user may not be using discovery
                logger.debug('%s - no discovery results %s', method, error);
            }

            // Orderers will be assigned to the channel by this point
            return this._commit(params.signed_envelope, timeout);
        } else {
            logger.debug('%s - using single orderer', method);
            // TODO set 'endorsingPeer'
            const orderer = this._channel._getOrderer(request.orderer, 'endorsingPeer');
            try {

                return orderer.sendBroadcast(params.signed_envelope, timeout);
            } catch (error) {
                logger.error(error.stack);
            }
            throw new Error('Failed to send to the orderer');
        }
    }

    async _commit(envelope, timeout) {
        const method = '_commit';

        let orderers = this._channel.getOrderers();
        //TODO shuffle orderers
        orderers = _.shuffle(orderers);
        let return_error = null;
        if (orderers && orderers.length > 0) {
            logger.debug('%s - found %s orderers assigned to channel', method, orderers.length);
            // loop through the orderers trying to complete one successfully
            for (const orderer of orderers) {
                logger.debug('%s - starting orderer %s', method, orderer.getName());
                try {
                    const results =  await orderer.sendBroadcast(envelope, timeout);
                    if (results) {
                        if (results.status === 'SUCCESS') {
                            logger.debug('%s - Successfully sent transaction to the orderer %s', method, orderer.getName());
                            return results;
                        } else {
                            logger.debug('%s - Failed to send transaction successfully to the orderer status:%s', method, results.status);
                            return_error = new Error('Failed to send transaction successfully to the orderer status:' + results.status);
                        }
                    } else {
                        return_error = new Error('Failed to send transaction to the orderer');
                        logger.debug('%s - Failed to send transaction to the orderer %s', method, orderer.getName());
                    }
                } catch (error) {
                    logger.debug('%s - Caught: %s', method, error.toString());
                    return_error = error;
                }

                logger.debug('%s - finished orderer %s ', method, orderer.getName());
            }

            logger.debug('%s - return error %s ', method, return_error.toString());
            throw return_error;
        } else {
            throw new Error('No orderers assigned to the channel');
        }
    }
}



module.exports = DiscoveryCommitHandler;