let FabricClient = require('fabric-client');
let _ = require('lodash');


let BaseFabricConnection = require("../BaseFabricConnection");
let DiscoveryEndorsementHandler = require('./DiscoveryEndorsementHandler');
let DiscoveryCommitHandler = require('./DiscoveryCommitHandler');
let logger = require('../logger');
let InternalError = require('../InternalError');


class ServiceDiscoveryProvider extends BaseFabricConnection {

    constructor(options) {
        super(options);
        this.channels = new Map();
        this.timeouts.discoveryCache = (options.timeouts && options.timeouts.discoveryCache) ? options.timeouts.discoveryCache : 60000;
        // It is a trick: We invoking refresh() manually in SetInterval (see below). So we don't need "lazy" update in Channel instance
        FabricClient.setConfigSetting('discovery-cache-life', this.timeouts.discoveryCache*1000);
    }

    start(options) {
        return super.init()
            .then(() => {
                if (this.tls) {
                    this.fabricClient.setConfigSetting('discovery-protocol', 'grpcs');
                } else {
                    this.fabricClient.setConfigSetting('discovery-protocol', 'grpcs');
                }

                if (options && options.asLocalhost) {
                    this.fabricClient.setConfigSetting('discovery-as-localhost', true);
                } else {
                    this.fabricClient.setConfigSetting('discovery-as-localhost', false);
                }

                let tasks = [];
                for (let channelId of Object.keys(this.connectionProfile.channels)) {
                    let channel = this.fabricClient.getChannel(channelId);

                    let discoveryTargets = [];
                    for (let peerName of Object.keys(this.connectionProfile.channels[channelId].peers)) {
                        let peerInfo = this.connectionProfile.channels[channelId].peers[peerName];
                        if (peerInfo.discover) {
                            discoveryTargets.push(peerName);
                        }
                    }
                    if (discoveryTargets.length === 0) {
                        throw new InternalError(`Connection profile doesn't have any peer with "discoveryTargets" property for channel ${channelId}`);
                    }
                    this.channels.set(channelId, {channelInstance: channel, discoveryTargets});

                    let task = channel.initialize({discover: true, target: _.sample(discoveryTargets)})
                        .then(() => {
                            logger.info(`Service discovery provider: channel ${channelId} successfully initialized`);
                        });
                    tasks.push(task);
                }
                return Promise.all(tasks);
            })
            .then(() => {
                // N.B. discovery result update is "lazy" (only when handler requests new results)
                setInterval(() => {
                    for (let {channelInstance, discoveryTargets} of this.channels.values()) {
                        this._refreshDiscoveryResults(channelInstance, discoveryTargets, false);
                    }
                }, this.timeouts.discoveryCache);
            });
    }

    _refreshDiscoveryResults(channel, discoveryTargets, retry) {
        if (retry) {
            // Trick: we are changing discovery peer
            let peer = this.fabricClient.getPeer(_.sample(discoveryTargets));
            channel._last_refresh_request.target = peer;
        }
        channel.refresh()
            .then(() => {
                logger.debug(`Channel ${channel.getName()} discovery results have been refreshed via peer ${channel._discovery_peer}`)
            })
            .catch((error) => {
                logger.error(`Errors occurred during refreshing discovery results for channel ${channel.getName()} via peer ${channel._discovery_peer}: ${error}`);
                logger.debug("Retrying ...");
                setTimeout(() => {
                    this._refreshDiscoveryResults(channel, discoveryTargets, true);
                }, 500);
            })
    }

    buildSendSignedProposalWithDiscoveryFunction() {
        // this function will be assigned to Channel instance
        return function(signedProposal, chaincodeId, timeout) {

            let endorsementHint = this._buildDiscoveryInterest(chaincodeId);
            const params = {
                signed_proposal: signedProposal,
                timeout: timeout,
                endorsement_hint: endorsementHint
            };
            return this._endorsement_handler.endorse(params);
        }
    }

    getEndorsementHandler(channel) {
        return DiscoveryEndorsementHandler.create(channel, this);
    }

    getCommitHandler(channel) {
        return DiscoveryCommitHandler.create(channel, this);
    }

    getChannelPeers(channelId) {
        let channel = this.channels.get(channelId).channelInstance;
        if (!channel) throw new InternalError(`Service discovery provider doesn't have channel ${channelId}`);
        return channel._channel_peers;
    }

    getChannelOrderers(channelId) {
        let channel = this.channels.get(channelId).channelInstance;
        if (!channel) throw new InternalError(`Service discovery provider doesn't have channel ${channelId}`);
        return channel._orderers;
    }

    getRandomOrderer(channelId) {
        let orderers = this.getChannelOrderers(channelId).values();
        let number = Math.floor(Math.random()*orderers.length);
        return orderers[number];
    }

    getDiscoveryResults(channelId) {
        let channel = this.channels.get(channelId).channelInstance;
        if (!channel) throw new InternalError(`Service discovery provider doesn't have channel ${channelId}`);
        return channel.getDiscoveryResults();
    }

    getEndorsementPlan(channelId, endorsement_hint) {
        let channel = this.channels.get(channelId).channelInstance;
        if (!channel) throw new InternalError(`Service discovery provider doesn't have channel ${channelId}`);
        return channel.getEndorsementPlan(endorsement_hint);
    }

}

module.exports = ServiceDiscoveryProvider;