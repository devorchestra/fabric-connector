let Promise = require('bluebird');
let superagent = require('superagent');
let request = require('request');


class Configtxlator {

    constructor(url, requestTimeout) {
        this.url = url;
        this.timeout = requestTimeout || 3000; // ms
    }

    encodeConfigToProtobuff(config) {
        return Promise.resolve()
            .then(() => {
                return new Promise((resolve, reject) => {
                    superagent.post(`${this.url}/protolator/encode/common.Config`, JSON.stringify(config))
                        .timeout({deadline: this.timeout})
                        .buffer()
                        .end((error, res) => {
                            if (error) {
                                return reject(error)
                            }
                            resolve(res.body);
                        });
                });
            });
    }

    decodeConfigProtobuff(configProtobuff) {
        return Promise.resolve()
            .then(() => {
                return new Promise((resolve, reject) => {
                    superagent.post(`${this.url}/protolator/decode/common.Config`, configProtobuff)
                        .timeout({deadline: this.timeout})
                        .buffer()
                        .end((error, res) => {
                            if (error) {
                                return reject(error)
                            }
                            resolve(res);
                        });
                });
            })
            .then((response) => {
                return JSON.parse(response.text.toString());
            })
    }

    decodeBlock(block) {
        return new Promise((resolve, reject) => {
            superagent.post(`${this.url}/protolator/decode/common.Block`, block.toBuffer())
                .timeout({deadline: this.timeout})
                .buffer()
                .end((error, res) => {
                    if (error) {
                        return reject(error)
                    }
                    resolve(res);
                });
        })
        .then((response) => {
            return JSON.parse(response.text.toString());
        });
    }

    generateCreateChannelProtobuff(config) {
        return new Promise((resolve, reject) => {
            superagent.post(`${this.url}/protolator/encode/common.ConfigUpdate`, JSON.stringify(config))
                .timeout({deadline: this.timeout})
                .buffer()
                .end((error, res) => {
                    if (error) {
                        return reject(error)
                    }
                    resolve(res.body);
                });
        });
    }

    generateUpdateConfigProtobuff(channelId, originalConfigProtobuff, updatedConfigProtobuff) {
        return Promise.resolve()
            .then(() => {
                let formData = {
                    channel: channelId,
                    original: {
                        value: originalConfigProtobuff,
                        options: {
                            filename: 'original.proto',
                            contentType: 'application/octet-stream'
                        }
                    },
                    updated: {
                        value: updatedConfigProtobuff,
                        options: {
                            filename: 'updated.proto',
                            contentType: 'application/octet-stream'
                        }
                    }
                };
                return new Promise((resolve, reject) => {
                    request.post({
                        url: `${this.url}/configtxlator/compute/update-from-configs`,
                        formData: formData,
                        encoding: null,
                        headers: {
                            accept: '/',
                            expect: '100-continue'
                        },
                        timeout: this.timeout
                    }, (error, res, body) => {
                        if (error) {
                            return reject(error);
                        } else if (res.statusCode !== 200) {
                            return reject(new Error(`Error in Configtxlator during generating update config. Status code: ${res.statusCode}, status message: ${res.statusMessage}`));
                        }
                        const proto = Buffer.from(body, 'binary');
                        resolve(proto);
                    });
                });
            });
    }

}

module.exports = Configtxlator;