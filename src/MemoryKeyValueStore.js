const fs = require("fs");

let MemoryKeyValueStore = class {
    constructor() {
        this.memoryStore = new Map();
        let self = this;
        return new Promise((resolve) => {
            resolve(self);
        })
    }
    getValue(name) {
        let value = this.memoryStore.get(name);
        return Promise.resolve(value);
    }
    setValue(name, value) {
        this.memoryStore.set(name, value);
        return Promise.resolve(name);
    }
};
module.exports = MemoryKeyValueStore;
