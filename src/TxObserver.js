let Promise = require('bluebird');
let _ = require('lodash');

let BaseFabricConnection = require("./BaseFabricConnection");
let logger = require('./logger');
let InternalError = require('./InternalError');

let MAX_RECONNECT_TRY = 8;

class TxObserver extends BaseFabricConnection {

    constructor(options) {
        super(options);
        this.channelEventHubs = new Map();
        this.channelsEventSourcePeers = new Map();
        this.disconnecting = false;
        this.timeouts.peerReconnection = (options.timeouts && options.timeouts.peerReconnection) ? options.timeouts.peerReconnection : 500;
    }

    startObserver() {
        return Promise.resolve()
            .then(() => {
                return this.init();
            })
            .then(() => {
                if (!this.connectionProfile.channels) {
                    throw new Error(`There is no any channel in connection profile, can't start TxObserver`);
                }

                for (let channelId of Object.keys(this.connectionProfile.channels)) {
                    let eventSourcePeers = [];
                    for (let peerName of Object.keys(this.connectionProfile.channels[channelId].peers)) {
                        let peerInfo = this.connectionProfile.channels[channelId].peers[peerName];
                        if (peerInfo.eventSource) {
                            eventSourcePeers.push(peerName);
                        }
                    }
                    if (eventSourcePeers.length === 0) {
                        throw new InternalError(`Connection profile doesn't have any peer with "eventSource" property for channel ${channelId}`)
                    }
                    this.channelsEventSourcePeers.set(channelId, eventSourcePeers);
                    this._connectToPeer(channelId, 0);
                }
            });
    }

    _connectToPeer(channelId, reconnectCounter) {
        let peerName = _.sample(this.channelsEventSourcePeers.get(channelId));
        let channelEventHub = this.fabricClient.getChannel(channelId).newChannelEventHub(peerName);
        this.channelEventHubs.set(channelId, channelEventHub);
        let isFullBlocks = false;
        // We do this trick to be able to catch stream error (error of a connection between us and a peer)
        channelEventHub.registerBlockEvent(
            () => {
                //stub
            },
            (reason) => {
                logger.info(`Transaction Observer for channel ${channelId} has been disconnected from peer ${peerName}: ${reason}`);
                if (this.disconnecting || reconnectCounter >= MAX_RECONNECT_TRY) {
                    return this.emit('closed', {channelId: this.channelId, reason});
                }

                this.emit('disconnected', {channelId: this.channelId, reason, peerName});
                logger.info(`Trying to reconnect in ${this.timeouts.peerReconnection} ms`);
                setTimeout(() => {
                    reconnectCounter++;
                    this._connectToPeer(channelId, reconnectCounter);
                }, this.timeouts.peerReconnection);
            }
        );

        channelEventHub.connect(isFullBlocks, (error) => {
            if (!error) {
                let msg = `Transaction Observer successfully subscribed to channel ${channelId} via peer ${peerName}`;
                logger.info(msg);
                this.emit('connected', {channelId, peerName});
                reconnectCounter = 0;
            } else {
                logger.error(`Errors occurred during connection a Transaction Observer to channel ${channelId} via peer ${peerName}`);
            }
        });
    }

    stopObserver() {
        for (let [channelId, channelEventHub] of this.channelEventHubs) {
            channelEventHub.disconnect();
            logger.info(`Transaction observer successfully disconnected from channel ${channelId}`);
        }
    }

    listenToTx(channelId, txId, timeout=30000) {
        return new Promise((resolve, reject) => {
            let channelEventHub = this.channelEventHubs.get(channelId);
            if (!channelEventHub) {
                return reject(new Error(`There is no EventHub (tx listener) for channel ${channelId}`));
            }
            let timeoutId = setTimeout(() => {
                channelEventHub.unregisterTxEvent(txId);
                reject(new Error(`Timeout: Failed to receive the event for tx ${txId} in channel ${channelId}`));
            }, timeout);
            channelEventHub.registerTxEvent(txId, (receivedTxId, code, blockNumber) => {
                clearTimeout(timeoutId);
                resolve({channelId, code, blockNumber, txId: receivedTxId});
            }, (error) => {
                clearTimeout(timeoutId);
                reject(error);
            });
        });
    }

    stopListenToTx(channelId, txId) {
        let channelEventHub = this.channelEventHubs.get(channelId);
        if (!channelEventHub) {
            throw new InternalError(`There is no EventHub (tx listener) for channel ${channelId}`);
        }
        channelEventHub.unregisterTxEvent(txId);
    }
}

module.exports = TxObserver;
