const plan = require('flightplan');
const fs = require('fs');

plan.target('development', {
    host: '34.243.172.132',
    username: 'ubuntu',
    agent: process.env.SSH_AUTH_SOCK
}, {
    dir: 'fabric-connector'
});


plan.local(function(local) {
    local.silent();
    local.log('uploading updates...');
    const filesToCopy = local.exec('git ls-files', {
        silent: true
    });
    local.transfer(filesToCopy, plan.runtime.options.dir);
});

plan.remote(function(remote) {

    remote["with"]("cd " + plan.runtime.options.dir, () => {
        remote.log('installing dependencies...');
        remote.exec('npm install');
    });
});